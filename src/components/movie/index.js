import React from 'react';
import './index.css';

const imdb = require('imdb-api');

export default class Movie extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            poster: ''
        };
    }

    componentDidMount() {
        this.fetchImage();
    }

    render() {
        return (
            <div className="four wide column">
                <div className="ui card">
                    <div className="content">
                        <div className="header">{this.props.movie.FullFilmTitle[0]}</div>

                        <p className="movieAttribute">
                            {this.props.movie.Attributes[0] != "" &&
                                <a className="ui blue image label">{this.props.movie.Attributes[0]}</a>
                            }
                        </p>
                    </div>

                    <div className="image">
                        <img width="100%" src={this.state.poster}/>
                    </div>

                    <div className="content">
                        <div className="description">
                            <p>
                                {this.props.movie.FilmSynopsisShort[0] + '...'}
                            </p>
                        </div>
                    </div>

                    <div className="extra content">
                        <div className="extra content">
                            <div className="ui list">
                                <div className="item">
                                    <i className="calendar icon"></i>
                                    <div className="content">
                                        {this.props.movie.ShowDateTime[0]}
                                    </div>
                                </div>
                                <div className="item">
                                    <i className="film icon"></i>
                                    <div className="content">
                                        {this.props.movie.CplexName[0]} ({this.props.movie.CinemaName[0]})
                                    </div>
                                </div>
                                <div className="item">
                                    <i className="flag icon"></i>
                                    <div className="content">
                                        {this.props.movie.FilmRatingCode[0]}
                                    </div>
                                </div>
                                <div className="item">
                                    <i className="user outline icon"></i>
                                    <div className="content">
                                        <strong>Seats Available:</strong> {this.props.movie.NoOfSeats[0]}
                                    </div>
                                </div>
                                <div className="item">
                                    <i className="user icon"></i>
                                    <div className="content">
                                        <strong>Seats Sold:</strong> {this.props.movie.NoOfSeatSold[0]}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    fetchImage() {
        let title = this.props.movie.FullFilmTitle[0];
        title = title.replace('2D', '');
        title = title.replace('3D', '');

        imdb.get(title, {apiKey: '8ca9ca08', timeout: 30000}).then((data) => {
            this.setState({poster: data.poster});
        }).catch((err) => {
            console.log(err);
        });
    }
}