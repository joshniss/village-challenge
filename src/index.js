import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// Import Components
import Movie from './components/movie';

// Import Semantic
import './semantic/dist/semantic.css';

// Import packages
import axios from 'axios';
import xml2js from 'xml2js';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            movies: []
        }

        this.loadData();
    }

    render() {
        return (
            <div className="ui container">
                <h1 style={{marginTop: '2em'}}>Playing Schedule</h1>

                <div className="ui stackable grid container" style={{marginTop: '2em'}}>
                    {this.renderMovies()}
                </div>
            </div>
        );
    }

    loadData() {
        var parser = new xml2js.Parser();

        axios.get('./data/SessionTimes.xml').then((response) => {
            parser.parseString(response.data, (err, result) => {
                this.setState({movies: result.ScheduleListDetail.Schedule});
                console.log(this.state.movies);
            });
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    renderMovies() {
        return this.state.movies.map((movie) => {
            return (
               <Movie movie={movie}/>
            )
        })
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);
